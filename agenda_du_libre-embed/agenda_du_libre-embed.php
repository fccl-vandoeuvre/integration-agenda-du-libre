<?php
/**
 * Plugin Name: Agenda du libre
 * Plugin URI: 
 * Description: Un plugin pour intégrer les évènements l'agenda du libre
 * Version: 2.5
 * Author: Fabian Wolf, David Libeau adapté par F. Floquet pour la Fabrique des possibles
 * Author 
 * License: GNU GPL v2 or later
 */

if( !class_exists( 'simple_html_dom' ) ) {
	require_once( plugin_dir_path(__FILE__) . 'simple_html_dom.php' );
}

class __agenda_du_libre_embed_plugin {
	public 	$pluginName = 'Agenda du libre',
			$pluginPrefix = 'agenda_du_libre_embed_';
	
	public 	$shortcode_name = 'agenda',
			$is_compatiblity_mode = false;
	/**
	 * Plugin instance.
	 *
	 * @see get_instance()
	 * @type object
	 */
	protected static $instance = NULL;

	/**
	 * Access this plugin’s working instance
	 *
	 * @wp-hook plugins_loaded
	 * @return  object of this class
	 */
	public static function get_instance() {

		NULL === self::$instance and self::$instance = new self( true );

		return self::$instance;
	}
	
	function __construct( $plugin_init = false ) {
		if( !empty( $plugin_init ) ) {
			if( function_exists( 'agenda_du_libre_embed_callback' ) ) {
				$this->is_compatiblity_mode = true;
				$this->shortcode_name = 'agenda_du_libre_embed';
			}

			$this->init_assets();
			
			add_shortcode( $this->shortcode_name, array( $this, 'shortcode' ) );
		}
	}
	
	function init_assets() {
	    $embed_content_style = apply_filters( $this->pluginPrefix . 'content_style', plugin_dir_url(__FILE__) . 'agenda_du_libre-embed.css' );
		$embed_content_style_dark = apply_filters( $this->pluginPrefix . 'content_style_dark', plugin_dir_url( __FILE__ ) . 'agenda_du_libre-embed-dark.css' );
		$embed_content_fa_url = apply_filters( $this->pluginPrefix . 'font_awesome_url', '//' );
		if( !empty( $embed_content_fa_url ) ) {
			wp_register_style( $this->pluginPrefix . 'font-awesome', $embed_content_fa_url );
            wp_enqueue_style( $this->pluginPrefix . 'font-awesome', $embed_content_fa_url );            
		}	
		if( !empty( $embed_content_style ) || !empty( $embed_content_style_dark ) ) {
			$strEmbedStyleURL = ( empty( $embed_content_style ) ? $embed_content_style_dark : $embed_content_style );					
			wp_register_style( $this->pluginPrefix . 'content', $strEmbedStyleURL );         
            wp_enqueue_style( $this->pluginPrefix . 'content', $strEmbedStyleURL );      
		}

/*		 
		$embed_js_helpers = apply_filters( $this->pluginPrefix . 'js_helpers_url', plugin_dir_url( __FILE__ ) . 'agenda_du_libre-embed.js' );	
		if( !empty( $embed_js_helpers ) ) {
			wp_register_script( $this->pluginPrefix . 'helpers', $embed_js_helpers, array('jquery' ), false, true );
            wp_enqueue_script( $this->pluginPrefix . 'helpers', $embed_js_helpers, array('jquery' ), false, true );            
		}
*/
	}
	
	function is_debug() {
		$return = false;
		if( ( defined( 'WP_DEBUG' ) && WP_DEBUG != false ) || current_user_can( 'manage_options' ) != false ) {
			$return = true;
		}
		if( defined( 'WP_DEBUG' ) && WP_DEBUG != false ) {
			if( ( defined('WP_DEBUG_LOG' ) && WP_DEBUG_LOG != false ) || ( defined( 'WP_DEBUG_DISPLAY' ) && WP_DEBUG_DISPLAY == false ) ) {
				$return = false;
			}
		}
		return $return;
	}
	
	function shortcode( $atts = null, $content = null ) {
		$return = '';
		$transient_name = 'agenda_status_'; 
		$debug = '';

		$default_atts = apply_filters( $this->pluginPrefix . 'default_attributes', array(
			'url' => '', 
			'container_class' => 'agenda-embed',
			'cache_timeout' => 24 * 60 * 60,
			'flush' => 0, // intentionally flush the cache  
			'nb_posts' => 10, // nombre de pouets par défaut peut être modifié ex :  [mastodon nb_posts="6" nb_cols="3"]https://framapiaf.org/@fccl_vandoeuvre[/mastodon]
			'largeur' => '300', // Largeur de colonne             
		) );
		
		$atts = shortcode_atts( $default_atts, $atts );
		
		extract( $atts, EXTR_SKIP );
		
		if( !empty( $content ) ) {
			$url = trim( strip_tags( $content ) );
		}
	
		if( !empty( $url ) ) {
			$url = esc_url( $url );
		}
			 
		if( !empty( $url ) ) {
			$url_hash = md5( $url );
			
			if( defined( '__AGENDA_CACHETIME' ) && empty( $cache_timeout ) ) {
				$cache_timeout = intval( __AGENDA_CACHETIME );     
			}
			
			$args = array(
				'sslverify' => false,
                'headers' => array( "Accept-Language" => "fr-fr;q=0.5",
                            )
			); 
			
			if( empty( $flush ) ) {
				$cache_response = get_transient( $transient_name . $url_hash );
			}
//$cache_response='';			
			if( empty( $cache_response ) ) {
				$response = wp_remote_get( $url, $args );
			
				set_transient( $transient_name . $url_hash, $response, $cache_timeout ); 
				
			} else {	 
				$response = $cache_response;
			}
            	
			$http_code = wp_remote_retrieve_response_code($response);
			$body = wp_remote_retrieve_body( $response );

			switch( $http_code ) {
				case 406:
					$container_class .= ' resource-not-found';
					$return = '<strong>Requête non acceptable.</strong>';
					break;			 
				case 404:
					$container_class .= ' resource-not-found';
					$return = '<strong>Ressource non trouvée.</strong>';
					break;
				case 301:
					$container_class .= ' resource-access-denied';
					$return = '<strong>Accès à la ressource refusé.</strong>';
					break;
				default:
					
					$dom = new simple_html_dom();
					$dom->load( $body );
                    
                    $ar_embed_content = $dom->find( 'article.event.fccl');                    
                    $host_url = parse_url( $url, PHP_URL_HOST );

/*                    
                    foreach($dom->find('a[class^=".event"]') as $act) { 
                        $act->target = "_blank";
                    }
*/                    
                    foreach($dom->find('a') as $act) { 
                        $act->target = "_blank";
                    }                    
                    
                    foreach($dom->find('img') as $img) { 
                      $img->class = "adl_img";
                      $img->style = "";
                    }
                      
                    $cpt=$nb_posts;  
                    foreach($ar_embed_content as $embed_content) {           
                        $posts.= '<div class="adl_post">'.$embed_content->outertext.'</div>';                            
                        $cpt--;
                        if ($cpt==0) break;
                    }  
                                         
                    $posts = str_replace('/events/', esc_url( '//' .$host_url .'/events/'), $posts);
                    $return = '<div class="adl_masonry" style="column-width:'.$largeur.'px;" >'.($posts).'</div>';
					break;
			}
		}

		if( empty( $return ) ) {
			$return = '<!-- agenda du libre: problème rencontré! ' . $debug . ' -->';
			$error = true;
		}		
		return $return;
	}
	 
	function is_empty_attr( $data, $empty_strings = array('null', 'false', '0' ) ) {
		$return = true;
		if( is_string( $data ) && $data != '' ) {
			if( in_array( strtolower( $data ), $empty_strings, true ) != false ) {
				$return = false;
			}
		}
		return $return;
	}

}

// init plugin
add_action( 'plugins_loaded', array( '__agenda_du_libre_embed_plugin', 'get_instance' ) );
