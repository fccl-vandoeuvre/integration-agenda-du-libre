# Integration Agenda du Libre

## Description

Extension pour Wordpress permettant d’intégrer des événements présents sur l’Agenda du Libre, dans une page statique.

Démonstration : https://fccl-vandoeuvre.fr/agenda

## Installation

> L'extension n'est pas disponible dans le gestionnaire intégré à Wordpress. Si vous connaissez les procédures, n'hésitez pas à l'ajouter !

- Accédez à votre serveur web via FTP/SFTP, copiez le dossier **agenda_du_libre-embed** dans **monsite/wp-content/plugins**.
- Activez l'extension dans Wordpress via le menu dédié.
- Intégrez dans votre page ce code court formaté en html : `[agenda nb_posts="20" largeur="300"]https://www.agendadulibre.org/tags/fccl[/agenda]`


## Personnalisation

Le code court précédent permet de personnaliser la source voulue ainsi que l'affichage global :
- **agenda nb_posts="20"** : Le nombre d'articles à afficher au maximum sur la page.
- **largeur="300"** : La largeur maximale de l'affichage d'un article en pixels.
- **https://www.agendadulibre.org/tags/fccl** : Le lien de l'agenda chargé en personnalisant le "tag" (ici fccl).


Pour aller plus loin, il est aussi possible d'éditer le code css pour personnaliser l'apparence (couleur, gras, cacher des éléments).
Éditez le fichier **monsite/wp-content/plugins/agenda_du_libre-embed/agenda_du_libre-embed.css**.

## Développement

La personne ayant mis à disposition ce code ne propose pas de support.
La [FCCL](https://fccl-vandoeuvre.fr) peut être en mesure d'apporter une aide à l'usage mais n'est pas qualifiée pour améliorer l'extension.
